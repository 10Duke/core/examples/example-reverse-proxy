# Reverse proxy for Web API sample applications

This repository is used to produce a docker image running the NGINX reverse proxy for a frontend
application ([code here](https://gitlab.com/10Duke/core/examples/example-webapp)) and one of a
number of example backends that use 10Duke SDK implementations to talk to 10Duke APIs.

The docker compose stacks for the backend projects are expected to map a self signed certificate and
key to the path `/nginx-selfsigned.crt` and `/nginx-selfsigned.key` respectively.

## Generating a self-signed certificate

Use `openssl` to generate a key pair.

```sh
    openssl req -x509 -newkey rsa:4096 \
      -keyout nginx-selfsigned.key \
      -out nginx-selfsigned.crt \
      -days 365 -nodes
```

You will be prompted for additional details for the key.

## Using the self-signed certificate

For example, using the image might look like:

```yaml
services:
  nginx:
    image: registry.gitlab.com/10duke/core/examples/example-reverse-proxy:latest
    volumes:
    - ${PWD}/cert:/cert
    - ${PWD}/cert/nginx-selfsigned.crt:/cert/nginx-selfsigned.crt
    - ${PWD}/cert/nginx-selfsigned.key:/cert/nginx-selfsigned.key
    ports:
      - "8090:80"
      - "8091:443"
    depends_on:
      - backend
      - frontend
```

assuming that a `cert` folder exists in the same folder that `docker compose` is run from and that
folder contains the generated self-signed certificate and key.
